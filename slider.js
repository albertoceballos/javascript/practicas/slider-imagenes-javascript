var fotos=new Array("1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg","9.jpg","10.jpg");
var imagenes=new Array();
var posicion=0;
window.addEventListener("load",()=>{
	document.querySelector(".galeria").style.left=0;
	//Precarga de imágenes
	for(var i=0;i<fotos.length;i++){
		imagenes[i]=new Image();
		imagenes[i].src="imgs/"+fotos[i];
	}
	//Dibujar un img para cada foto precargada y los enlaces para cada una de ellas
	for(var c=0;c<fotos.length;c++){
		document.querySelector(".galeria").innerHTML+='<img class="thumbs" src="imgs/'+fotos[c]+'" data-indice="'+c+'"></img>';
		document.querySelector(".enlacesThumbs").innerHTML+='<a href="#" class="enlaces" data-indice="'+c+'">'+parseInt(c+1)+'</a>'+' ';
	}
	//asignar funcion para cargar foto a cada thumbnail
	var thumbs=document.querySelectorAll(".thumbs");
	var enlaces=document.querySelectorAll(".enlaces")
	for(i=0;i<thumbs.length;i++){
		thumbs[i].addEventListener("click", cargarFoto);
		enlaces[i].addEventListener("click", cargarFoto);
	}

	document.querySelector("#anterior").addEventListener("click",anterior);
	document.querySelector("#siguiente").addEventListener("click",siguiente);
	document.querySelector("#flechaDrcha").addEventListener("mouseover",iniciaCarruselDrcha);
	document.querySelector("#flechaDrcha").addEventListener("mouseout", stopCarruselDrcha);
	document.querySelector("#flechaIzq").addEventListener("mouseover",iniciaCarruselIzq);
	document.querySelector("#flechaIzq").addEventListener("mouseout", stopCarruselIzq);

	console.log(thumbs);
});
	function cargarFoto (event) {
		document.querySelector("#marco").style.opacity = '1.0';
		var indice=event.target.getAttribute("data-indice");
		document.querySelector("#marco").style.backgroundImage="url('"+imagenes[indice].src+"')";
		posicion=indice;
		console.log("Posición: "+posicion);
	}

	function anterior(){
		if(posicion>0){
			posicion--;
			document.querySelector("#marco").style.backgroundImage="url('"+imagenes[posicion].src+"')";	
		}
	}
	function siguiente(){
		if(posicion<imagenes.length-1){
			posicion++;
			console.log("pulsado siguiente"+posicion);
			document.querySelector("#marco").style.backgroundImage="url('"+imagenes[posicion].src+"')";
		}
	}

	function iniciaCarruselDrcha(){
		intervalo1=setInterval(function(){
			var pos=parseInt(document.querySelector(".galeria").style.left);
			if(pos>-1700){
				document.querySelector(".galeria").style.left=pos-10+"px";
				console.log(pos);
			}
		},10);
	}

	function stopCarruselDrcha(){
		clearInterval(intervalo1);
	}

	function iniciaCarruselIzq(){
			intervalo2=setInterval(function(){	
				var pos=parseInt(document.querySelector(".galeria").style.left);
				if(pos<0){
					document.querySelector(".galeria").style.left=pos+10+"px";
					console.log(pos);
				}
			},10);
	}

	function stopCarruselIzq(){
		clearInterval(intervalo2);
	}

